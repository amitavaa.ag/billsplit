class BillSerializer < ApplicationSerializer
  fields :description, :split_type, :settled, :people

  association :payee, blueprint: UserSerializer
  association :user_bills, blueprint: UserBillSerializer
end