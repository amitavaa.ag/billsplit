class UserSerializer < ApplicationSerializer
  fields :email, :currency, :created_at
end
