class PaymentSerializer
  def self.render_as_hash(payments)
    owes_to   = UserBillSerializer.render_as_hash(payments[:owes_to], view: :payee)
    owes_from = UserBillSerializer.render_as_hash(payments[:owes_from], view: :payer)

    return { owes: owes_to, receives: owes_from }
  end
end