class UserBillSerializer < ApplicationSerializer
  fields :settled, :payer_id, :paid

  field :payable do |user_bill|
    suffix = user_bill.bill.percentage? ? "%" : ""
    [user_bill.payable, suffix].join
  end

  field :percentage do |user_bill|
    "#{user_bill.percentage}%"
  end

  field :total_amount do |user_bill|
    user_bill.bill.total_amount
  end
  
  view :payer do
    association :payer, blueprint: UserSerializer
  end

  view :payee do
    association :payee, blueprint: UserSerializer
  end

  view :detailed do
    include_view :payer
    include_view :payee
  end
end