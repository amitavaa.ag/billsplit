class UserBill < ApplicationRecord
  belongs_to :bill
  belongs_to :payer, class_name: 'User', foreign_key: :payer_id

  has_one :payee, through: :bill, source: :user

  validate :settlement
  validate :payable_amount, on: :update

  def self.generate(members)
    params = [
      :payable,
      :paid,
      :bill_id,
      :payer_id,
      :settled
    ]

    UserBill.transaction do
      members.map do |member|
        UserBill.create!(member.slice(*params))
      end
    end
  end

  def percentage
    ((paid.to_d / bill.total_amount.to_d) * 100.to_d).round(3)
  end

  def payee?(user_id)
    payee.id == user_id
  end

  def payers
    UserBill.joins(:bill).where(bill_id: bill_id).where("user_bills.payer_id != ?", payer_id)
  end

  private

  def settlement
    errors.add(:paid, "cannot exceed payable") unless Money.gt?(payable.to_d, paid.to_d, bill.total_amount, bill.split_type)
  end

  def payable_amount
    _payable, total_amount =  bill.percentage? ? [payable, 100.to_d] : [self.payable, bill.total_amount]

    would_be_total = BigDecimal(_payable) + BigDecimal(payers.sum(&:payable))

    errors.add(:payable, "disrupts bill total amount") unless Money.equal?(would_be_total, total_amount)
  end
end
