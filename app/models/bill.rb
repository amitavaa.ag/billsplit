class Bill < ApplicationRecord
  belongs_to :user, foreign_key: 'payee_id'
  belongs_to :payee, class_name: 'User'
  
  has_many :user_bills
  has_many :payers, through: :user_bills

  validates :total_amount, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :description, presence: true, length: { in: 1..120 }

  before_save :set_type, :validate_amount

  enum split_type: %i(absolute percentage share)

  scope :for_user, ->(user_id) {  joins(:user).includes(:user).where(payee_id: user_id) }

  def settle!
    update(settled: true)
  end

  class << self
    def method_missing(method, *args)
      if Bill.split_types.keys().map { |t| "#{t}_type"}.include? method.to_s
        method.to_s =~ /(\w+)_type/
        "#{$1}"
      end
    end

    def respond_to_missing?(method, *)
      method =~ /(\w+)_type/ || super
    end
  end


  private

  def set_type
    self.split_type = split_type.nil? ? :absolute : split_type
  end

  def validate_amount
    split_amount = user_bills.sum(&:payable)
    errors.add(:total_amount, "total amount does not add up") unless Money.equal?(split_amount, total_amount)
  end
end

