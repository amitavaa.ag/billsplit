# user.rb

class User < ApplicationRecord
  validates :email, :digest, presence: true
  validates :email, uniqueness: true, format: { with: /\A\S+@\S+\.\S+\Z/ }
  validates :currency, inclusion: { in: Currency.all.map(&:name) }, allow_nil: true

  has_secure_password
  alias_attribute :password_digest, :digest

  has_many :bills, foreign_key: 'payee_id'
  has_many :user_bills, through: :bills

  before_save :set_currency

  def password=(password)
    self.digest = BCrypt::Password.create(password)
  end

  def valid_password?(password)
    BCrypt::Password.new(digest).is_password?(password)
  end

  private

  def set_currency
    self.currency = Currency.default unless currency
  end
end
