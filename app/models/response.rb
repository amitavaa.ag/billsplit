class Response
  def initialize(status: nil, errors: [], code: nil, data: {})
    @errors = errors
    @data = data
    @code = code
    @status = status
  end

  def self.errors(status: Codes::INTERNAL_ERROR, errors: [], code: nil)
    new(status: status, errors: errors, code: code)
  end

  def self.success(data: {})
    new(status: :ok, data: data)
  end
end
