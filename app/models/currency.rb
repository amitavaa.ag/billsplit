class Currency
  CurrencyStruct = Struct.new(:id, :name)
  @currencies = %w[inr usd].map.with_index { |cur, index| CurrencyStruct.new(index + 1, cur) }

  def self.all
    @currencies
  end

  def self.default
    'inr'
  end
end
