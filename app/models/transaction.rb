class Transaction
  attr_reader :user, :owes_to, :owes_from

  def initialize(user)
    @user = user
  end

  def payments
    { owes_to: owes_to, owes_from: owes_from }
  end

  def owes_to
    @owes_to ||= UserBill
    .joins(:bill)
    .includes(:payer, :bill)
    .where(payer_id: @user.id, settled: false).where(bills: { settled: false })
  end

  def owes_from
    @owes_from ||= UserBill
    .joins(:bill)
    .includes(:bill, :payer)
    .where(bills: { payee_id: @user.id , settled: false })
    .where(user_bills: { settled: false })
  end

  def self.settle!(bill)
    billsplits = bill.user_bills.select(:paid, :settled)

    total_to_pay = billsplits.sum(&:paid)
    total_amount = bill.absolute? ? bill.total_amount : BigDecimal('100.0')
    all_settled = billsplits.map(&:settled).reduce(true) { |acc, v| acc = acc && v }

    bill.update!(settled: true) if Money.equal?(total_to_pay, bill.total_amount) or all_settled
  end
end
