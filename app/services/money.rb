class Money
  def self.equal?(value1, value2, precision=3)
    diff = value1.to_d - value2.to_d

    return diff.round(3) == BigDecimal('0.0')
  end

  def self.percentage_payable(receive_amount, total_amount)
    ratio = receive_amount.to_d / total_amount.to_d
    ratio * BigDecimal('100')
  end

  def self.settled?(payable, receive_amount, total_amount, type)
    if type == Bill.percentage_type
      equal?(percentage_payable(receive_amount, total_amount), payable)
    elsif type == Bill.absolute_type
      equal?(payable, receive_amount)
    end
  end

  # greater of payable and received
  def self.gt?(payable, receive_amount, total_amount, type)
    if type == Bill.percentage_type
      ppayable = percentage_payable(total_amount, receive_amount)
      ppayable.round(3) > payable.to_d.round(3)
    else
      payable.round(3) > receive_amount.to_d.round(3)
    end
  end
end