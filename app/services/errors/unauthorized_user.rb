module Errors
  class UnauthorizedUser < ::StandardError
    attr_reader :code

    def initialize(msg='user.unauthorized')
      @code = Codes::UNAUTHORIZED

      super(msg)
    end
  end
end
