module Errors
  class InvalidAuthenticationHeader < InvalidAuthentication
    attr_reader :code

    def initialize(code: Codes::AUTH_HEADER_ISSUES)
      @type = self.class.name.downcase.to_sym
      @code = code

      super(msg: 'auth.headers.missing', code: code)
    end
  end
end