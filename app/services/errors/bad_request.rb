module Errors
  class BadRequest < ::StandardError
    attr_reader :code

    def initialize(msg: 'bad.request', code: nil)
      @code = code

      super(msg)
    end
  end
end