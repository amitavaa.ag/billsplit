module Errors
  class InvalidSplits < StandardError
    attr_reader :code
    
    def initialize(msg: 'bill.splits.invalid', code: Codes::INVALID_BILL_SPLIT)
      @code = code

      super(msg)
    end
  end
end