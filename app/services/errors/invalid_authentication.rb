module Errors
  class InvalidAuthentication < ::StandardError
    attr_reader :code

    def initialize(msg: 'auth.header.error', code: nil)
      @type = self.class.name.downcase.to_sym
      @code = code

      super(msg)
    end
  end
end