module Splittings
  class DifferentialSplitting < BaseService
    def initialize(bill, members)
      super(bill, members)
    end

    def splits
      @splits ||= @members.map do |member|
        h = { payable: member[:payable].round(3).to_d, paid: 0, bill_id: bill.id, payer_id: member[:id] }
        h.merge!(paid: member[:payable].round(3).to_d, settled: true) if payee?(member[:id])
        h
      end
    end

    def validate!
      super

      raise Errors::InvalidSplits.new(msg: 'bill.splits.payer.missing', code: Codes::PAYEE_NOT_IN_SPLIT) unless payee_in_members?
    end

    private
    
    def payee_in_members?
      payee = @members.find { |member| member[:id].to_s == @bill.payee.id.to_s }
      payee.present? && payee[:payable].present?
    end
  end
end
