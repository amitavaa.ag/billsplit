module Splittings
  class BaseService
    attr_reader :bill, :splits

    def initialize(bill, members)
      @bill = bill
      @members = members
    end

    def splits
      raise NotImplementedError
    end

    def validate!
      raise Errors::InvalidSplits.new(msg: 'bill.splits.duplicate.payer', code: Codes::DUPLICATE_PAYERS) unless uniq?
      raise Errors::InvalidSplits.new(msg: 'bill.splits.invalid.values', code: Codes::TOTAL_AMOUNT_MISMATCH) unless amount_split_valid?

      true
    end

    private

    def uniq?
      uniq_members = @members.uniq { |member| member[:id].to_s }
      uniq_members.length == @members.length
    end

    def payee?(id)
      bill.payee_id.to_s == id.to_s
    end

    def amount_split_valid?
      ::AmountSplitValidator.new(bill, @members.map { |member| member[:payable] }).valid?
    end
  end
end
