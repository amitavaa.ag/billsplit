module Splittings
  class EqualSplitting < BaseService
    attr_reader :payable_amount

    def initialize(bill, members)
      @bill = bill
      @members = normalize_payments(members)

      super(bill, @members)
    end

    # def payable_amount
    #   @payable_amount ||= BigDecimal(bill.total_amount) / BigDecimal(@members.length)
    # end

    def splits
      @splits ||= @members.map do |member|
        h = { payable: payable_amount, paid: 0, bill_id: bill.id, payer_id: member[:id] }
        h.merge!(paid: payable_amount, settled: true) if payee?(member[:id])
        h
      end
    end

    def normalize_payments(members)
      @payable_amount ||= BigDecimal(@bill.total_amount) / BigDecimal(members.length)

      has_payee = members.find { |member| member[:id].to_s == bill.payee_id.to_s }.present?
      members.push({ id: @bill.payee.id.to_s }) if not has_payee

      @payable_amount ||= (BigDecimal(@bill.total_amount) / BigDecimal(members.length)).round(3)

      members.map do |member|
        member.merge!(payable: payable_amount)
      end
    end
  end
end