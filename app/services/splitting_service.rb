class SplittingService
  def self.builder(bill, members, equally)
    @bill = bill
    @members = members

    if equally
      return Splittings::EqualSplitting.new(@bill, @members)
    end

    Splittings::DifferentialSplitting.new(@bill, @members)
  end

  def self.members_with_payee(members)
    if members.find { |member| member[:id].to_s == @bill.payee_id.to_s }.present?
      return members
    end

    members.push({ id: @bill.payee_id.to_s })
  end

  private_class_method :members_with_payee
end
