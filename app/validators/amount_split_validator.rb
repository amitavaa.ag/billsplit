class AmountSplitValidator
  attr_reader :bill

  def initialize(bill, amounts)
    @bill = bill
    @amounts = amounts
  end

  def valid?
    total_split_amount = @amounts.reduce(BigDecimal('0')) { |acc, amount| acc += amount.to_d }

    return false if bill.percentage? && total_split_amount > BigDecimal('100.0')
    return false if bill.absolute? && !Money.equal?(total_split_amount, bill.total_amount)

    return true
  end
end