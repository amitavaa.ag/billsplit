class ApplicationController < ActionController::API
  include ExceptionHandler
  include Authenticator

	def ping
		render json: {}, status: :ok
	end

  private

  def authenticate_and_set_current_user
    validate_auth_headers!

    @current_user ||= authenticate_with_http_basic do |email, password|
      user = User.find_by!(email: email)
      raise ::Errors::InvalidAuthentication unless user.valid_password? password
      user
    end
  end
end
