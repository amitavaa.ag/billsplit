class Api::UsersController < ApplicationController
  before_action :authenticate_and_set_current_user, except: :signup

  def signup
    user = User.create(signup_params)
    
    if user.valid?
      render json: Response.success(
        data: UserSerializer.render_as_hash(user)
        ), status: :created
    else
      render json: Response.errors(status: :bad_request, errors: user.errors.messages), status: :bad_request
    end
  end

  def search
    users = User.limit(20)
    render json: Response.success(data: UserSerializer.render_as_hash(users)), status: :ok
  end

  def validate
    render json: Response.success(data: UserSerializer.render_as_hash(@current_user))
  end

  private

  def signup_params
    params.permit([:email, :password, :currency])
  end
end

