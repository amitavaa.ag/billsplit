class Api::BillsController < ApplicationController
  before_action :authenticate_and_set_current_user
  before_action :authorized?, only: [:update, :index]

  def create
    bill = Bill.create!(bill_params)
   
    service = SplittingService.builder(bill, members, split_equally?)
    service.validate!

    user_bills = UserBill.generate(service.splits)

  rescue Errors::InvalidSplits => error
    bill.destroy
    raise error
  else
    render json: Response.success(
      data: BillSerializer.render_as_hash(bill.reload)
      ), status: :created
  end

  def index
    bills = Bill.includes(:user).for_user(@current_user.id)

    render json: Response.success(
      data: BillSerializer.render_as_hash(bills, user_bills: { view: :detailed })
      ), status: :ok
  end

  def update
    bill = Bill.find_by(id: params[:id], payee_id: @current_user.id)
    bill.update!(description: params[:description])
  end

  private

  def bill_params
    params.permit([
      :description,
      :total_amount,
      :payee_id
    ]).merge(split_type: split_type, people: members.length)
  end

  def members
    @members ||= params.fetch(:splits, []).map { |split| split.merge(id: split[:id].to_s) }
  end

  def split_equally?
    params.fetch(:split_equally, false)
  end

  def split_type
    split_type = if params[:percentage] == true
      :percentage
    else
      :absolute
    end

    Bill.split_types[split_type]
  end
end