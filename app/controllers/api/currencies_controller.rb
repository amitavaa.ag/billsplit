class Api::CurrenciesController < ApplicationController
  def index
    currencies = Currency.all
    render json: Response.success(
      data: CurrencySerializer.render_as_hash(currencies)
      ), status: :ok
  end
end
