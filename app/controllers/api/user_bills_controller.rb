class Api::UserBillsController < ApplicationController
  before_action :authenticate_and_set_current_user, :authorized?
  before_action :members_authorized?, :payee_settling?, :payer_settling?, :validate_settlement, only: :update

  def index
    transaction = Transaction.new(@current_user)
    render json: Response.success(
      data: PaymentSerializer.render_as_hash(transaction.payments)
      ), status: :ok
  end

  def update
    update_params = update_payment_params
    update_params = update_params.merge(paid: amount) if update_params[:paid].present?
    update_params = update_params.merge(settled: settled?) unless update_params[:settled].present?

    user_bill.with_lock do
      user_bill.reload

      previous_paid = user_bill.paid
      update_params = update_params.merge(paid: previous_paid + update_params[:paid])


      user_bill.update!(update_params)
    end

    Transaction.settle!(user_bill.bill)

    render json: Response.success(
      data: UserBillSerializer.render_as_hash(UserBill.find(user_bill.id))
      ), status: :ok
  end

  private

  def update_payment_params
    params.permit([
      :payer_id,
      :payable,
      :paid,
      :settled
    ])
  end

  def amount
    params.fetch(:paid, 0).to_d
  end

  def user_bill
    @user_bill ||= UserBill.includes(bill: :payers).joins(bill: :payers).find_by!(id: params[:id])
  end

  def bill
    @bill = user_bill.bill
  end

  def members_authorized?
    raise Errors::UnauthorizedUser.new('not.allowed.settle') unless user_bill.bill.payers.include?(@current_user)
  end

  def payee_settling?
    payable_or_settling = update_payment_params[:payable].present? || update_payment_params[:settled].present?

    raise Errors::UnauthorizedUser.new('not.allowed.to.update') if payable_or_settling && !user_bill.payee?(@current_user.id)
  end

  def payer_settling?
    raise Errors::UnauthorizedUser.new('not.allowed.to.settle') unless [user_bill.payer, user_bill.payee].include?(@current_user)
  end

  def validate_settlement
    raise Errors::BadRequest.new(msg: 'bill.settled', code: Codes::ALREADY_SETTLED) if user_bill.settled?
  end

  def settled?
    Money.settled?(user_bill.payable, user_bill.paid + amount, bill.total_amount, bill.split_type)
  end
end
