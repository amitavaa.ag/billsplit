module ExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from StandardError, with: :server_error

    rescue_from ::Errors::InvalidAuthentication,

                ::Errors::UnauthorizedUser,
                with: :unauthorized

    rescue_from ActiveRecord::RecordNotFound,
                with: :not_found

    rescue_from ::Errors::BadRequest,
                ::Errors::InvalidSplits,
                ActionController::ParameterMissing,
                ActiveModel::UnknownAttributeError,
                ActiveModel::ValidationError,
                ActiveRecord::RecordInvalid,
                ActiveRecord::RecordNotUnique,
                with: :bad_request
  end


  private

  def unauthorized(error)
     render_error(:unauthorized, error, error.message, error.try(:code))
  end

  def bad_request(error)
    render_error(:bad_request, error, error.try(:message), error.try(:code))
  end

  def not_found(error)
    render_error(:not_found, error, 'invalid resource' , error.try(:code))
  end

  def server_error(error)
    render_error(:internal_server_error, error, 'something went wrong', '500')
  end

  def render_error(status, error, message, code)
    logging_enabled = %w[development test].include?(Rails.env.downcase)

    puts error.backtrace.slice(0, 10).join("\n") if logging_enabled
    puts error.message if logging_enabled

    render json: Response.errors(
        status: status,
        errors: [message],
        code: code
      ), status: status
  end

end