module Authenticator
  extend ActiveSupport::Concern

  included do
    include ActionController::HttpAuthentication::Basic::ControllerMethods

    def validate_auth_headers!
      raise ::Errors::InvalidAuthenticationHeader unless auth_header.present?
      raise ::Errors::InvalidAuthenticationHeader unless auth_header.starts_with?('Basic')
    end

    def authorized?(user_id = params[:user_id])
     raise ::Errors::UnauthorizedUser unless @current_user.present?
     raise ::Errors::UnauthorizedUser unless @current_user.id.to_s == user_id.to_s
    end
  end

  private

  def auth_header
    request.headers['Authorization']
  end
  
end