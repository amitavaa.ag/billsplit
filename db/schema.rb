# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_18_193256) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bills", force: :cascade do |t|
    t.string "description", limit: 120
    t.decimal "total_amount", null: false
    t.integer "people", default: 1, null: false
    t.boolean "settled", default: false
    t.integer "split_type", limit: 2
    t.bigint "payee_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["payee_id"], name: "index_bills_on_payee_id"
  end

  create_table "user_bills", force: :cascade do |t|
    t.decimal "payable"
    t.decimal "paid"
    t.bigint "bill_id", null: false
    t.bigint "payer_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "settled", default: false
    t.index ["bill_id"], name: "index_user_bills_on_bill_id"
    t.index ["payer_id"], name: "index_user_bills_on_payer_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email", null: false
    t.string "digest", null: false
    t.string "currency", limit: 4
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "bills", "users", column: "payee_id"
  add_foreign_key "user_bills", "bills"
  add_foreign_key "user_bills", "users", column: "payer_id"
end
