class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email, null: false, index: { unique: true }
      t.string :digest, null: false
      t.string :currency, limit: 4
      t.timestamps
    end
  end
end
