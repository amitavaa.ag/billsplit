class CreateBills < ActiveRecord::Migration[6.0]
  def change
    create_table :bills do |t|
      t.string  :description, limit: 120
      t.decimal :total_amount, null: false
      t.integer :people, null: false, default: 1
      t.boolean :settled, default: false
      t.integer :split_type, limit: 2

      t.references :payee, foreign_key: { to_table: 'users' }, index: true, null: false
      t.timestamps
    end
  end
end

