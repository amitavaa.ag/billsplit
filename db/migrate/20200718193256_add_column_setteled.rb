class AddColumnSetteled < ActiveRecord::Migration[6.0]
  def change
    add_column :user_bills, :settled, :boolean, default: false
  end
end
