class CreateUserBills < ActiveRecord::Migration[6.0]
  def change
    create_table :user_bills do |t|
      t.decimal :payable
      t.decimal :paid

      t.references :bill, foreign_key: true, index: true, null: false
      t.references :payer, foreign_key: { to_table: 'users' }, null: false, index: true

      t.timestamps
    end
  end
end
