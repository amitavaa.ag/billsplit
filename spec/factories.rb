FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "test+#{n}@test.com" }
    password { 'somepass' }
  end


  factory :bill do
    user

    description { 'Test' }
    total_amount { 1000 }
    payee_id { user.id }
    people { 3 }
    settled { false }

    trait :settled do
      settled { true }
    end
  end

  factory :user_bill do
    bill

    payer { bill.user }
    bill_id { bill.id }
    payable { 1000 }
    paid { 0 }
    settled { false }

    trait :settled do
      paid { payable }
      settled { true }
    end
  end

end
