require 'rails_helper'

RSpec.describe Transaction, unit: true do
  let(:users)  { create_list(:user, 3) }
  let(:amount) { 1500 }
  
  describe '.settle' do
    it 'should declare bill as settled' do
      bill = create(:bill, user: users[1], total_amount: amount, settled: false)

      create(:user_bill, bill: bill, paid: 750)
      create(:user_bill, bill: bill, payer: users[0], paid: 750)

      Transaction.settle!(bill)
      bill.reload

      expect(bill.settled).to eq(true)
    end
  end

  describe '#payments' do
    it 'should return the bills owed by the user' do
      user_1, user_2, user_3 = users

      # bill for user 2
      bill_2 = create(:bill, user: user_2, total_amount: amount)

      create(:user_bill, :settled, bill: bill_2, payable: 500)
      create(:user_bill, bill: bill_2, payer: user_1, payable: 300)
      create(:user_bill, bill: bill_2, payer: user_3, payable: 700)

      # bill for user 1
      bill_1 = create(:bill, user: user_1, total_amount: amount)

      create(:user_bill, :settled, bill: bill_1, payable: 500)
      create(:user_bill, bill: bill_1, payer: user_2, payable: 700)
      create(:user_bill, bill: bill_1, payer: user_3, payable: 300)

      # settled bill for user 3
      bill_settled = create(:bill, :settled, user: user_3, total_amount: amount)

      create(:user_bill, :settled, bill: bill_settled, payable: 300)
      create(:user_bill, :settled, bill: bill_settled, payer: user_2, payable: 300)
      create(:user_bill, :settled, bill: bill_settled, payer: user_1, payable: 300)

      payments = Transaction.new(user_1).payments
      
      owes_to   = payments[:owes_to]
      owes_from = payments[:owes_from]

      expect(owes_to.count).to eq 1
      expect(owes_from.count).to eq 2

      expect(owes_to.first.payee).to eq(user_2)
      expect(owes_from.map(&:payer)).to include(user_2, user_3)
    end
  end
end