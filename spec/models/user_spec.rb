require 'rails_helper'

RSpec.describe User, type: :model do
  context 'creation' do
    it 'creates account with email and excrypted password' do
      User.create!(email: 'test@test.com', password: 'somepass')
      user = User.last
      
      expect(user.password).to be nil
      expect(user.digest).not_to be nil
    end

    it 'fails for invalid email and empty password' do
      errors = User.create(email: 'test').errors

      expect(errors.details).to include(:email, :digest)
    end

    it 'validates password' do
      User.create!(email: 'test@test.com', password: 'somepass')

      expect(User.last.valid_password?('somepass')).to be true
    end

    it 'fails for duplicate email' do
      user = create(:user)
      new_user = User.create(email: user.email, password: 'test')

      expect(new_user.valid?).to be false
      expect(new_user.errors).to include(:email)
    end
  end
end

