require 'rails_helper'

RSpec.describe Money, type: :unit do
  describe '.equal?' do
    it 'equals to decimals rounded to 3 decimal places' do
      x = 2000.0/3
      y = '666.6666667'
      expect(Money.equal?(x, y)).to be true
    end
  end

  describe '.settled?' do
    it 'settles decimal differences' do
      x = 2000/3
      y = '666.67'
      expect(Money.settled?(x, y, 0, Bill.absolute_type)).to be true
    end

    it 'settles percentage differences' do
      total = 2000.0
      paid = 600
      payable_in_percent = 30.0

      expect(Money.settled?(total, paid, payable_in_percent, Bill.percentage_type)).to be true
    end

    it 'returns false when invalid' do
    end
  end
end
