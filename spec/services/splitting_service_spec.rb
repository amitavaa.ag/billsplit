require 'rails_helper'

RSpec.describe SplittingService, unit: true do
  describe '#splitbill' do
    let(:users)   { create_list(:user, 3) }
    let(:bill)    { create(:bill, user: users[0], total_amount: BigDecimal(2000)) }
    let(:members) { [{ id: users[0].id, payable: 1000 }, { id: users[1].id, payable: 500 }, { id: users[2].id, payable: 500 }] }

    let(:equal_splitter)   { SplittingService.builder(bill, members, true) }
    let(:unequal_splitter) { SplittingService.builder(bill, members, false) }

    it 'splits equally when equally is true' do 
      equal_splitter.validate!

      splits = equal_splitter.splits
      amount = BigDecimal(bill.total_amount / 3)

      expect(splits.map { |split| split[:payable] }).to eq([amount, amount, amount])

      payee = splits.find { |split| split[:payer_id] == bill.payee_id }
      expect(payee[:paid]).to eq(payee[:payable])

      payer = splits.detect { |split| split[:payer_id] != bill.payee_id }
      expect(payer[:paid]).to eq(0)
    end

    it 'splits unequally' do
      unequal_splitter.validate!

      splits = unequal_splitter.splits

      payee = splits.find { |split| split[:payer_id] == bill.payee_id }
      expect(payee[:paid]).to eq(1000.to_d)
      expect(payee[:payable]).to eq(1000.to_d)

      payer = splits.detect { |split| split[:payer_id] != bill.payee_id }
      expect(payer[:paid]).to eq(0)
    end

    it 'throws validations when splitting unequally like a stupid being' do
      members[0][:payable] = bill.total_amount + 100

      expect { unequal_splitter.validate! }.to raise_error(Errors::InvalidSplits)
    end
  end

end