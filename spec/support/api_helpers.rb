module ApiHelpers
  def json_body
    JSON.parse(response.body, symbolize_names: true)
  end

  def with_auth_env(email, password)
    header = yield
    env = { 'HTTP_AUTHORIZATION' => ActionController::HttpAuthentication::Basic.encode_credentials(email, password) }
    header.merge(env)
  end
end

RSpec.configure do |config|
  config.include ApiHelpers, type: :request
end
