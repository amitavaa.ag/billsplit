require 'rails_helper'

RSpec.describe Api::UserBillsController, type: :request do
  let(:users) { create_list(:user, 3, password: 'password') }
  let(:bill)  { create(:bill, user: users[0], total_amount: 2000.01) }
  let(:headers) { { 'Content-Type' => 'application/json' } }

  describe 'PUT /api/users/:user_id/payments/:id' do
    context 'with payer' do
      before do
        @headers = with_auth_env(users[1].email, 'password'){ headers }
      end

      before(:each) do
        create(:user_bill, :settled, bill: bill, payer: users[0], payable: 1000.0, paid: 1000.0)
        @ub = create(:user_bill, bill: bill, payer: users[1], payable: 1000.0, paid: 1000.0)
      end
  
      it 'settles split when an user pays up' do
        expect(@ub.settled).to be false

        put "/api/users/#{users[1].id}/payments/#{@ub.id}", headers: @headers, params: { paid: 1000.0 }.to_json

        expect(json_body[:data][:settled]).to be true
        expect(UserBill.find(@ub.id).settled).to be true

        expect(Bill.find(bill.id).settled).to be true
      end

      it 'only updates paid to paid value' do
        expect(@ub.settled).to be false

        put "/api/users/#{users[1].id}/payments/#{@ub.id}", headers: @headers, params: { paid: 100.0 }.to_json

        user_bill = UserBill.find(@ub.id)

        expect(json_body[:data][:settled]).to be false
        expect(user_bill.paid).to eq(100.0)
        expect(user_bill.settled).to be false
        
        expect(Bill.find(bill.id).settled).to be false
      end

      it 'returns error when updating payable' do
        put "/api/users/#{users[1].id}/payments/#{@ub.id}", headers: @headers, params: { payable: 100.0 }.to_json

        expect(response).to have_http_status :unauthorized
      end

      it 'returns error when paid amount exceeds payable' do
        put "/api/users/#{users[1].id}/payments/#{@ub.id}", headers: @headers, params: { paid: 10000.0 }.to_json

        expect(response).to have_http_status :ok
      end

    end

    context 'with payee' do
      before do
        @headers = with_auth_env(users[0].email, 'password'){ headers }
      end

      before(:each) do
        create(:user_bill, :settled, bill: bill, payer: users[0], payable: 1000.0, paid: 1000.0)
        @ub = create(:user_bill, bill: bill, payer: users[1], payable: 1000.0, paid: 1000.0)
      end

      it 'allows bill split to be settled manually' do
        put "/api/users/#{users[0].id}/payments/#{@ub.id}", headers: @headers, params: { settled: true }.to_json

        expect(response).to have_http_status :ok
      end

      it 'returns error when someone else is accessing' do
        @headers = with_auth_env(users[2].email, 'password'){ headers }

        put "/api/users/#{users[0].id}/payments/#{@ub.id}", headers: @headers, params: { settled: true }.to_json

        expect(response).to have_http_status :unauthorized
      end
    end
  end
end