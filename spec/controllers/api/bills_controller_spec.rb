require 'rails_helper'

RSpec.describe Api::BillsController, type: :request do
  let(:user) { create(:user) }
  let(:another_user) { create(:user, password: 'lololol') }
  let(:headers) { { 'Content-Type' => 'application/json' } }

  context 'validations' do
    describe 'failure' do
      it 'returns error if credentials invalid' do
        get "/api/users/#{user.id}/bills", headers: with_auth_env(another_user.email, 'lololol'){ headers }

        expect(response).to have_http_status :unauthorized
      end
    end
  end

  describe 'GET /api/users/:user_id/bills' do
    let!(:bills) { create_list(:bill, 2, user: user) }

    before do
      @headers = with_auth_env(user.email, 'somepass'){ headers }
    end

    context 'success' do
      it 'returns bills for user' do
        get "/api/users/#{user.id}/bills", headers: @headers

        expect(response).to have_http_status :ok

        bills_response = json_body[:data]

        expect(bills_response.count).to eq(2)
        expect(bills_response.map { |resp| resp[:payee][:id] }).to eq([user.id, user.id])
      end
    end
  end

  describe 'POST /api/bills' do
    let(:params) {
      {
        description: "TestBill",
        total_amount: 2000,
        percentage: false,
        payee_id: user.id,
        split_equally: false,
        splits: [
          {
            id: user.id,
            payable: 1000
          },
          {
            id: another_user.id,
            payable: 500
          }
        ]
      }
    }

    let(:user_3) { create(:user) }

    before do
      @headers = with_auth_env(user.email, 'somepass'){ headers }
    end

    context 'failures' do
      it 'returns error when distribution does not add up' do
        post '/api/bills', headers: @headers, params: params.to_json

        expect(response).to have_http_status :bad_request
        expect(json_body[:code]).to eq(Codes::TOTAL_AMOUNT_MISMATCH)
      end

      it 'returns error when duplicate members in splits' do
        params[:splits][1][:id] = user.id

        post '/api/bills', headers: @headers, params: params.to_json

        expect(response).to have_http_status :bad_request
        expect(json_body[:code]).to eq(Codes::DUPLICATE_PAYERS)
      end

      it 'returns error when payee not in split for split unequally' do
        params[:splits][0][:id] = user_3.id
        params[:splits][1][:payable] = 1000

        post '/api/bills', headers: @headers, params: params.to_json

        expect(response).to have_http_status :bad_request
        expect(json_body[:code]).to eq(Codes::PAYEE_NOT_IN_SPLIT)
      end
    end

    context 'success' do
      it 'creates bills with splits equally and settle for payee' do
        params[:split_equally] = true

        post '/api/bills', headers: @headers, params: params.to_json

        expect(response).to have_http_status :created

        resp = json_body[:data]

        expect(resp[:id]).not_to be_nil
        expect(resp[:user_bills].map { |ubill| ubill[:payable] }).to contain_exactly("1000.0", "1000.0")

        payee = resp[:user_bills].find { |ubill| ubill[:payer_id] == user.id }
        expect(payee[:settled]).to eq(true)
      end

      it 'creates bills with different splits amounts and settles for payee' do
        params[:splits][0][:payable] = 1500

        post '/api/bills', headers: @headers, params: params.to_json

        expect(response).to have_http_status :created

        resp = json_body[:data]

        expect(resp[:user_bills].map { |ubill| ubill[:payable] }).to contain_exactly("1500.0", "500.0")

        payee = resp[:user_bills].find { |ubill| ubill[:payer_id] == user.id }
        payer = resp[:user_bills].find { |ubill| ubill[:payer_id] == another_user.id }

        expect(payee[:settled]).to eq(true)
        expect(payer[:paid]).to eq("0.0")
      end
    end
  end

  describe 'PUT /api/users/:user_id/bills/:id' do
    let(:bill)   { create(:bill, user: user) }
    let(:params) { { description: "Changed" } }

     before do
      @headers = with_auth_env(user.email, 'somepass'){ headers }
    end

    context 'failure' do
      it 'raises error for bill not for user' do
        put "/api/users/#{another_user.id}/bills/#{bill.id}", headers: @headers, params: params.to_json

        expect(response).to have_http_status :unauthorized
      end
    end
  end
end