require 'rails_helper'

RSpec.describe Api::UsersController, type: :request do
  let(:headers) { { 'Content-Type': 'application/json' } }

  describe 'POST /users/signup' do
    it 'creates an user with valid details' do
      params = { email: "test@test.com", password: "password" }.to_json

      post '/api/users/signup', params: params, headers: headers

      expect(response).to have_http_status(:created)

      user = json_body[:data]

      expect(user[:email]).to eq('test@test.com')
      expect(user[:currency]).to eq(Currency.default)
    end

    it 'returns error on duplicate email' do
      user = create(:user)

      post '/api/users/signup', params: { email: user.email }.to_json, headers: headers

      expect(response).to have_http_status(:bad_request)

      errors = json_body[:errors]
      expect(errors[:email][0]).to match('taken')
      expect(errors[:password][0]).to match('blank')
    end
  end
end

