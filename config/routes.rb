Rails.application.routes.draw do
	get '/ping', to: 'application#ping'

	namespace :api, constraints: { format: 'json' } do
    	post 'users/signup', to: 'users#signup'
      post 'users/authtest', to: 'users#validate'
      get 'users/search', to: 'users#search'
	
      resources :currencies, only: [:index]
      resources :bills, only: [:create]

      resources :users, only: [] do
        resources :bills, only: [:index, :get, :update]
      end

      get 'users/:user_id/payments', to: 'user_bills#index', as: 'net_payments'
      put 'users/:user_id/payments/:id', to: 'user_bills#update', as: 'settle_payament'
  end
end
